const puppeteer = require('puppeteer')

const args = Object.assign({
    'slide-interval': 5,
    'window-position': '0,0',
    'reload-interval': 600,
    'no-fullscreen': false,
}, require('args-parser')(process.argv))

console.log('Arguments:\n',args)

for (let k of ['slide-interval', 'reload-interval'])
    args[k] *= 1000

var state = {
    slidesN: 0,
    lastReload: 0
}

function sleep(ms) {
    return new Promise(resolve => {
        setTimeout(resolve, ms)
    })
}

async function promiseTimeout(prom, ms) {
    rejectProm = new Promise((_, reject) => {
        setTimeout(() => {
            reject('Promise timed out')
        }, ms)
    })
    return await Promise.race([prom, rejectProm])
}

async function getIFrame(page, match) {
    let attempts = 0    
    while (attempts++ < 90) {
        for (let frame of await page.frames()) {
            if (await frame.name() == 'wac_frame')
                return frame
        }
        
        if (attempts > 4)
            await sleep(1000 * 60 * 5)
        else
            await sleep(20)
    }

    throw('wac_frame not found, is anyone logged in?')
}

async function getSlideshowFrame(page) {
    for (let frame of await page.frames()) {
        let queryRes = await frame.$('#SlidePanel')
        if (!queryRes)
            continue

        console.log('SlidePanel found:', queryRes)
        return frame
    }
    throw("Couldn't find slideshow iframe :(")
}

async function setVisibility(page, state) {
    console.log('setting visibility', state)

    await page.evaluate(
        (state) => {
            var target = document.body.querySelector('#WebApplicationFrame')
            if (!target.style.transition)
                target.style.transition = 'opacity 1s linear'

            target.style.opacity = state ? '1' : '0'
            setTimeout(() => {
                target.style.visiblility = state ? 'visible' : 'hidden'
            }, 1000)
        }, 
        state
    )
    await sleep(1100)
}

async function reloadSlides(page) {
    console.log('reloading slides')

    await setVisibility(page, false)
    await page.reload()
    state.lastReload = Date.now()

    await sleep(1000)
}

async function present(page) {
    console.log('starting presentation')

    let frame = await getIFrame(page)
    await sleep(2000)

    let thumbnails = await frame.$$('div.ThumbnailContainer')
    state.slidesN = thumbnails.length

    await frame.click('#RibbonTopBarContainer .fui-SplitButton__primaryActionButton')
}

async function returnToBeginning(page) {
    console.log('returning to beginning')
    // await setVisibility(page, false)

    let slideshow = await getIFrame(page)

    await slideshow.evaluate(() => {
        document.querySelector('#slideshow-toolbar').style.display = null
        document.querySelector('#gridButton').click()
        document.querySelector('#slideshow-toolbar').style.display = 'none'

        return new Promise((resolve) => {
            setTimeout(() => {
                document.querySelector('div[id^=grid-frame-view] .generic-view-class').click()
                resolve()
            }, 1000)
        })
    })
    await sleep(2000)
    // let gridBtn = await slideshow.$('#gridButton')
    // console.log(gridBtn)
    // await gridBtn.hover()
    // await sleep(1000)
    // await gridBtn.click()

    // let firstSlide = await slideshow.$('div[id^=grid-frame-view]')
    // await firstSlide.click()

    // await setVisibility(page, true)
}

async function runSlideshow(page) {
    console.log('Beginning slides')
    let slideshow = await getSlideshowFrame(page)

    await setVisibility(page, true)
    await sleep(args['slide-interval'])

    for (let i=1; i < state.slidesN; i++ ) {
        console.log('Advancing slide')

        await slideshow.evaluate(() => {
            document.querySelector('#SlidePanel').click()
        })

        await sleep(args['slide-interval'])
    }
    console.log('done with slideshow')
}

async function run(page) {
    await present(page)
    await sleep(5000)

    while (true) {
        await runSlideshow(page)

        if (Date.now() - state.lastReload > args['reload-interval']) {
            await reloadSlides(page)
            await present(page)
        } else {
            await returnToBeginning(page)
        }
    }
}

async function loop() {
    var browser
    try {
        let puppeteerArgs = [
            `--window-position=${args['window-position']}`,
            '--disable-session-crashed-bubble',
            '--disable-features=InfiniteSessionRestore'
        ]

        if (!args['no-fullscreen'])
            puppeteerArgs.push('--start-fullscreen')

        console.log(puppeteerArgs)

        browser = await puppeteer.launch({
            headless: false,
            args: puppeteerArgs,
            ignoreDefaultArgs: ["--enable-automation"],
            userDataDir: './user_data',
            defaultViewport: null,
        })
        let page = await browser.newPage()

        let pages = await browser.pages()
        await pages[0].close()

        console.log('Navigating to', args.url)
        await page.goto(args.url)

        state.lastReload = Date.now()

        // await setVisibility(page, true)
        // while (true) {
        //     await runSlideshow(page)
        //     await reloadSlides(page)
        // }
        await run(page)
    } catch(e) {
        console.error('error :(')
        console.error(e)

        if (typeof browser != 'undefined' && browser.close)
            browser.close()

        console.log('Reloading app in 15 seconds')
        await sleep(15000)
    }
}

async function main() {
    while (true) {
        await loop()
    }
}
main()